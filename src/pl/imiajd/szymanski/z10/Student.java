package pl.imiajd.szymanski.z10;

import java.time.LocalDate;
import java.util.Objects;

public class Student extends Osoba {
    private final double sredniaOcen;
    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen){
        super(nazwisko,dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }
    @Override
    public int compareTo(Osoba o){
        Student student = (Student) o;
        int result = super.compareTo(o);
        if (result == 0){
            result = Double.compare(this.sredniaOcen, student.sredniaOcen);
        }
        return result;
    }
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return super.equals(o) && Objects.equals(this.sredniaOcen, student.sredniaOcen);
    }
    @Override
    public String toString(){
        return this.getClass().getSimpleName()+" [ "+ getNazwisko()+", "+ getDataUrodzenia()+", "+sredniaOcen+" ]";
    }
}
