package pl.imiajd.szymanski;
import java.time.LocalDate;

public abstract class Instrument {
    private final String producent;
    private final LocalDate rokProdukcji;
    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }
    public String getProducent(){return producent;}
    public LocalDate getRokProdukcji(){return rokProdukcji;}
    public abstract void dzwiek();
    public boolean equals(Object a){
        if(this == a){return true;}
        if(a == null){return false;}
        if(this.getClass() != a.getClass()){return false;}
        Instrument other = (Instrument) a;
        return this.producent.equals(other.producent) && this.rokProdukcji == other.rokProdukcji;
    }
    @Override
    public String toString(){
        return "producent: " + this.producent + ", rok produkcji: " + this.rokProdukcji;
    }
}
