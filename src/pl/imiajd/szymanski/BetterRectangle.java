package pl.imiajd.szymanski;

public class BetterRectangle extends java.awt.Rectangle {
    public BetterRectangle(int width, int height){
        super(width, height);
    }
    public int getPerimeter(){
        return 2 * width + 2 * height;
    }
    public int getArea(){
        return width * height;
    }
}
