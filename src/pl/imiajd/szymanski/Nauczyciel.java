package pl.imiajd.szymanski;

public class Nauczyciel extends Osoba {
    private final int pensja;

    public Nauczyciel(String nazwisko, int rok_urodzenia, int pensja) {
        super(nazwisko, rok_urodzenia);
        this.pensja = pensja;
    }

    public String toString() {
        return super.getNazwisko() + " " + super.getRok_urodzenia() + " " + pensja;
    }

    public int getPensja() {
        return pensja;
    }
}
