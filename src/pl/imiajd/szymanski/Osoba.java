package pl.imiajd.szymanski;

public class Osoba {
    private final String nazwisko;
    private final int rok_urodzenia;

    public Osoba(String nazwisko, int rok_urodzenia){
        this.nazwisko = nazwisko;
        this.rok_urodzenia = rok_urodzenia;
    }

    public String toString(){
        return nazwisko + " " + rok_urodzenia;
    }

    public String getNazwisko(){
        return nazwisko;
    }

    public int getRok_urodzenia(){
        return rok_urodzenia;
    }
}
