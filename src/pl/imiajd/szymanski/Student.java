package pl.imiajd.szymanski;


public class Student extends Osoba {
    private final String kierunek;
    public Student(String nazwisko, int rok_urodzenia, String kierunek){
        super(nazwisko,rok_urodzenia);
        this.kierunek = kierunek;
    }
    public String toString(){
        return super.getNazwisko() + " " + super.getRok_urodzenia() + " " + kierunek;
    }
    public String getKierunek(){
        return kierunek;
    }
}
