package pl.imiajd.szymanski;

public class Adres {
    private final String ulica, miasto;
    private final int numer_domu, kod_pocztowy;
    private int numer_mieszkania;

    public Adres(String kod_pocztowy, String miasto, String ulica, int numer_domu){
        String[] kodsplit = kod_pocztowy.split("-",2);
        this.kod_pocztowy = Integer.parseInt(kodsplit[0] + kodsplit[1]);
        this.miasto = miasto;
        this.ulica = ulica;
        this.numer_domu = numer_domu;
    }

    public Adres(String kod_pocztowy, String miasto, String ulica, int numer_domu, int numer_mieszkania){
        String[] kodsplit = kod_pocztowy.split("-",2);
        this.kod_pocztowy = Integer.parseInt(kodsplit[0] + kodsplit[1]);
        this.miasto = miasto;
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
    }

    public void pokaz(){
        String kod = Integer.toString(kod_pocztowy).substring(0,2) + "-" + Integer.toString(kod_pocztowy).substring(2,5);
        System.out.println(kod + " " + miasto);
        if(numer_mieszkania == 0){
            System.out.println(ulica + " " + numer_domu);
        }
        else {
            System.out.println(ulica + " " + numer_domu + "/" + numer_mieszkania);
        }

    }

    public boolean przed(Adres a){
        return kod_pocztowy < a.kod_pocztowy;
    }
}
