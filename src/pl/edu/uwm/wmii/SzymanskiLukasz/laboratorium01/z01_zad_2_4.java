package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium01;

import java.util.Scanner;

public class z01_zad_2_4 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        double max=Double.NEGATIVE_INFINITY,min=Double.POSITIVE_INFINITY;
        for (int i = 1; i <= n; i++) {
            float liczba=in.nextFloat();
            if(liczba>max){
                max=liczba;
            }
            if (liczba<min){
                min=liczba;
            }

        }
        System.out.println("najwieksza : "+max);
        System.out.println("najmniejsza : "+min);

    }
}