package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium01;

import java.util.Scanner;

public class z01_zad_2_1_e {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int ilosc=0;
        for (int i = 1; i <= n; i++) {
            int liczba=in.nextInt();
            long silnia=1;
            for (int j=1;j<=i;j++){
                silnia*=j;
            }
            if(liczba>Math.pow(2,i) && liczba<silnia){
                ilosc+=1;
            }
        }
        System.out.println(ilosc);
    }
}