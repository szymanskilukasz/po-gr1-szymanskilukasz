package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium01;

import java.util.Scanner;

public class z01_zad_2_3 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int dodatnie=0,ujemne=0,zera=0;
        for (int i = 1; i <= n; i++) {
            float liczba=in.nextFloat();
            if(liczba>0){
                dodatnie+=1;
            }
            else if(liczba<0){
                ujemne+=1;
            }
            else {
                zera+=1;
            }
        }
        System.out.println("dodatnie : "+dodatnie);
        System.out.println("ujemne : "+ujemne);
        System.out.println("zera : "+zera);
    }
}