package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium01;

import java.util.Scanner;

public class z01_zad_2_1_c {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int ilosc=0;
        for (int i = 1; i <= n; i++) {
            int liczba=in.nextInt();
            if(Math.sqrt(liczba)%2==0){
                ilosc+=1;
            }
        }
        System.out.println(ilosc);
    }
}