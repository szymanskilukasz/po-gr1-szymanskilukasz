package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium01;

import java.util.Scanner;

public class z01_zad_1_2 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        float[] tab;
        tab=new float[n];
        for (int i = 0; i < n; i++) {
            tab[i]=in.nextFloat();
        }
        System.out.println();
        for(int i=1;i<n;i++){
            System.out.println(tab[i]);
        }
        System.out.println(tab[0]);
    }
}
