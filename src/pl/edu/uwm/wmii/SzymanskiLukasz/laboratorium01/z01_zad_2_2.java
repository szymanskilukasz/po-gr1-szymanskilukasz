package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium01;

import java.util.Scanner;

public class z01_zad_2_2 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int suma=0;
        for (int i = 1; i <= n; i++) {
            int liczba=in.nextInt();
            if(liczba>0){
                suma+=liczba*2;
            }
        }
        System.out.println(suma);
    }
}