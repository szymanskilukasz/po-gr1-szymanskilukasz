package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium01;

import java.util.Scanner;

public class z01_zad_1_1_b {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        double suma=1;
        for (int i = 1; i <= n; i++) {
            suma*=in.nextFloat();
        }
        System.out.println(suma);
    }
}