package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium01;

import java.util.Scanner;

public class z01_zad_1_1_a {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        float suma=0;
        for (int i = 1; i <= n; i++) {
            suma+=in.nextFloat();
        }
        System.out.println(suma);
    }
}
