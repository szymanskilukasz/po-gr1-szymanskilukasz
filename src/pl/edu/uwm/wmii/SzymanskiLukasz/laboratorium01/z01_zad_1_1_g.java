package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium01;

import java.util.Scanner;

public class z01_zad_1_1_g {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        float podany;
        float suma1=0;
        double suma2=1;
        for (int i = 1; i <= n; i++) {
            podany=in.nextFloat();
            suma1+=podany;
            suma2*=podany;
        }
        System.out.println(suma1);
        System.out.println(suma2);
    }
}