package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium01;

import java.util.Scanner;

public class z01_zad_1_1_i {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        float suma=0;
        for (int i = 1; i <= n; i++) {
            long silnia=1;
            for (int j=1;j<=i; j++){
                silnia*=j;
            }
            if (i%2==0){
                suma+=(in.nextFloat()/silnia);
            }
            else {
                suma-=(in.nextFloat()/silnia);
            }
        }
        System.out.println(suma);
    }
}