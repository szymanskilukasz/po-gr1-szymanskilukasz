package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium08;

import pl.imiajd.szymanski.z08.Osoba;
import pl.imiajd.szymanski.z08.Pracownik;
import pl.imiajd.szymanski.z08.Student;

import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];
        String[] imiona0 = new String[]{"Jan", "Marek"};
        LocalDate dataur0 = LocalDate.of(1985,9,12);
        LocalDate dataz0 = LocalDate.of(1997,2,20);
        String[] imiona1 = new String[]{"Małgorzata"};
        LocalDate dataur1 = LocalDate.of(1998,11,21);

        ludzie[0] = new Pracownik("Kowalski",imiona0 ,dataur0, true, 50000, dataz0);
        ludzie[1] = new Student("Małgorzata Nowak",imiona1, dataur1, false, "Informatyka",4.2);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}







