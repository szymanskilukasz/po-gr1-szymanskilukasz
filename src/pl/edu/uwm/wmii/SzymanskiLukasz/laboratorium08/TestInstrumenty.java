package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium08;

import pl.imiajd.szymanski.Flet;
import pl.imiajd.szymanski.Fortepian;
import pl.imiajd.szymanski.Instrument;
import pl.imiajd.szymanski.Skrzypce;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args) {
        Flet flet1 = new Flet("yamaha", LocalDate.of(1997,1,11));
        Flet flet2 = new Flet("firma fletów", LocalDate.of(2010,9,2));
        Skrzypce skrzypce1 = new Skrzypce("firma skrzypiec",LocalDate.of(2011,10,30));
        Skrzypce skrzypce2 = new Skrzypce("violin",LocalDate.of(2000,12,10));
        Fortepian fortepian = new Fortepian("Casio", LocalDate.of(1995,5,19));
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(flet1);
        orkiestra.add(flet2);
        orkiestra.add(skrzypce1);
        orkiestra.add(skrzypce2);
        orkiestra.add(fortepian);

        for (Instrument instrument : orkiestra) {
            instrument.dzwiek();
        }
        System.out.println("\nSkład Orkiestry:");
        for (Instrument instrument : orkiestra) {
            System.out.println(instrument);
        }
    }
}
