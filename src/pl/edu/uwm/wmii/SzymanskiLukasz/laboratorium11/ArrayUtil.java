package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium11;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class ArrayUtil {

    public static <T extends Comparable<? super T>> boolean isSorted(ArrayList<T> tab){
        ArrayList<T> help= new ArrayList<>(tab);
        Collections.sort(help);
        return tab.equals(help);
    }

    public static <T extends Comparable<? super T>> int binSearch(ArrayList<T> tab, T a){
        if(ArrayUtil.isSorted(tab)){
            int left = 0;
            int right = tab.size() - 1;
            while (left <= right) {
                int middle = left + (right - left) / 2;
                if (tab.get(middle).equals(a))
                    return middle;
                else if (tab.get(middle).compareTo(a)<0)
                    left = middle + 1;
                else
                    right = middle - 1;
            }
        }
        return -1;
    }

    public static <T extends Comparable<? super T>> void selectionSort(ArrayList<T> arr){
        int top;
        T temp;
        for (int i=arr.size()-1; i>=0 ;i--)
        {
            top=0;
            for (int j=0; j<=i; j++)
            {
                if (arr.get(top).compareTo(arr.get(j))<0)
                {
                    top=j;
                }
            }

            if (top!=i)
            {
                temp=arr.get(i);
                arr.set(i,arr.get(top));
                arr.set(top,temp);

            }
        }
    }

    public static <T extends Comparable<? super T>>  void mergeSort(ArrayList<T> arr){
        if (arr.size() > 1) {

            ArrayList<T> left = new ArrayList<>();
            ArrayList<T> right = new ArrayList<>();

            boolean logicalSwitch = true;

            while (!arr.isEmpty()) {

                if (logicalSwitch) {
                    left.add(arr.remove(0));
                } else {
                    right.add(arr.remove(arr.size()/2));
                }

                logicalSwitch = !logicalSwitch;
            }
            mergeSort(left);
            mergeSort(right);

            while (!left.isEmpty() && !right.isEmpty()) {

                if(left.get(0).compareTo(right.get(0)) <= 0){
                    arr.add(left.remove(0));
                }
                else {
                    arr.add(right.remove(0));
                }

            }
            if(!left.isEmpty()){

                arr.addAll(left);

            }
            else if (!right.isEmpty()){

                arr.addAll(right);

            }
        }
    }

    public static void main(String[] args){
        ArrayList<LocalDate> daty = new ArrayList<>();
        daty.add(LocalDate.of(1961,1,26));
        daty.add(LocalDate.of(1950,2,26));
        daty.add(LocalDate.of(2011,3,17));
        System.out.print("Before:" + isSorted(daty) + "\n");
        System.out.print("1961,1,26 at:" + binSearch(daty, LocalDate.of(1961,1,26)) + " = not sorted \n");
        mergeSort(daty);
        System.out.print("After:" + isSorted(daty) + "\n");
        System.out.print("1961,1,26 now at:" + binSearch(daty, LocalDate.of(1961,1,26)) + "\n");

        ArrayList<Integer> liczby = new ArrayList<>();
        liczby.add(-1);
        liczby.add(2);
        liczby.add(263);
        liczby.add(7);
        System.out.print("Before:" + isSorted(daty) + "\n");
        System.out.print("number '263' at:" + binSearch(liczby, 263) +" = not sorted\n");
        selectionSort(liczby);
        System.out.print("After:" + isSorted(daty) + "\n");
        System.out.print("number now '263' at:" + binSearch(liczby, 263) + "\n");


    }

}
