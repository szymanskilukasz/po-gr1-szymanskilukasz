package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium10;

import pl.imiajd.szymanski.z10.Osoba;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>();
        Osoba o1 = new Osoba("Kowalski", LocalDate.of(1997,10,3));
        Osoba o2 = new Osoba("Kowalski", LocalDate.of(1994,2,3));
        Osoba o3 = new Osoba("Zielony", LocalDate.of(1995,10,2));
        Osoba o4 = new Osoba("Czerwony", LocalDate.of(1995,10,2));
        Osoba o5 = new Osoba("Biały", LocalDate.of(1996,10,5));
        Osoba ja = new Osoba("Szymański",LocalDate.of(1997,11,21));
        grupa.add(o1);
        grupa.add(o2);
        grupa.add(o3);
        grupa.add(o4);
        grupa.add(o5);
        grupa.add(ja);

        for(Osoba osoba : grupa){
            System.out.println(osoba);
        }
        System.out.println("\npo sortowaniu");
        grupa.sort(Osoba::compareTo);
        for(Osoba osoba : grupa){
            System.out.println(osoba);
        }
    }
}
