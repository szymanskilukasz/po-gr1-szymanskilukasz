package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium10;

import pl.imiajd.szymanski.z10.Student;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Student> grupa = new ArrayList<>();
        Student o1 = new Student("Kowalski", LocalDate.of(1997, 10, 3), 3.2);
        Student o2 = new Student("Kowalski", LocalDate.of(1994, 2, 3), 3.4);
        Student o3 = new Student("Zielony", LocalDate.of(1995, 10, 2), 4.2);
        Student o4 = new Student("Czerwony", LocalDate.of(1995, 10, 2), 4.6);
        Student o5 = new Student("Biały", LocalDate.of(1996, 10, 5), 5.0);
        Student ja = new Student("Szymański", LocalDate.of(1997, 11, 21), 3.5);
        grupa.add(o1);
        grupa.add(o2);
        grupa.add(o3);
        grupa.add(o4);
        grupa.add(o5);
        grupa.add(ja);

        for (Student student : grupa) {
            System.out.println(student);
        }
        System.out.println("\npo sortowaniu");
        grupa.sort(Student::compareTo);
        for (Student student : grupa) {
            System.out.println(student);
        }
    }
}