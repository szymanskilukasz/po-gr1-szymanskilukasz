package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class z10_zad_3 {
    public static void main(String[] args) throws IOException {
        String path = new File("src\\pl\\edu\\uwm\\wmii\\SzymanskiLukasz\\laboratorium10").getAbsolutePath();
        ArrayList<String> lista = new ArrayList<>();
        String plik = "plik.txt";
        BufferedReader reader = new BufferedReader(new FileReader(path+"\\"+plik));
        String line = reader.readLine();
        while (line != null){
            lista.add(line);
            line = reader.readLine();
        }
        reader.close();
        for(String s : lista){
            System.out.println(s);
        }
        Collections.sort(lista);
        System.out.println("\nPo Sortowaniu:");
        for(String s : lista){
            System.out.println(s);
        }
    }
}
