package pl.edu.uwm.wmii.SzymanskiLukasz.Kolokwium02;

import java.util.ArrayList;
import java.util.List;

public class Przepis {
    private String nazwa;
    private double suma = 0;
    private List<Ingredient> ingredienci = new ArrayList<>();
    private int czasPrzygotowania;

    public void DodajIngredient(Ingredient i){
        ingredienci.add(i);
        suma += i.getCenaIngredienta();
    }

    public void UstawNazweICzas(String nazwa, int czas){
        this.nazwa = nazwa;
        this.czasPrzygotowania = czas;
    }

    @Override
    public String toString() {
        String string;
        StringBuilder s = new StringBuilder();
        s.append("Przepis: ").append(nazwa).append("\n");
        ingredienci.sort(Ingredient::compareTo);
        for(Ingredient ingredient : ingredienci){
            s.append(ingredient).append("\n");
        }
        s.append("Suma: ").append(suma);
        string = s.toString();
        return string;
    }

    public boolean CzyCzas(){
        return czasPrzygotowania > 0;
    }

    public int IleIngredientow(){
        return ingredienci.size();
    }
}
