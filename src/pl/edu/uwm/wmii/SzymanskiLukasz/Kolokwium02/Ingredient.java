package pl.edu.uwm.wmii.SzymanskiLukasz.Kolokwium02;


public class Ingredient implements Comparable<Ingredient> {
    private final String nazwaIngredienta, ilosc;
    private final double cenaIngredienta;

    public Ingredient(String nazwaIngredienta, String ilosc, double cenaIngredienta) {
        this.nazwaIngredienta = nazwaIngredienta;
        this.ilosc = ilosc;
        this.cenaIngredienta = cenaIngredienta;
    }

    public String toString() {
        return "Nazwa: " + nazwaIngredienta + ", Ilość: " + ilosc + ", Cena: " + cenaIngredienta;
    }

    public double getCenaIngredienta() {
        return cenaIngredienta;
    }

    @Override
    public int compareTo(Ingredient o) {
        return this.nazwaIngredienta.compareTo(o.nazwaIngredienta);
    }
}
