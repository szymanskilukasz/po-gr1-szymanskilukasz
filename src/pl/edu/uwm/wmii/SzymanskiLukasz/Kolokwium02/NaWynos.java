package pl.edu.uwm.wmii.SzymanskiLukasz.Kolokwium02;

import java.time.LocalDateTime;

public class NaWynos extends Zamowienie {
    @Override
    public boolean PoprawnyCzas() {
        LocalDateTime za2h = LocalDateTime.now().plusHours(2);
        int result = czasDostawy.compareTo(za2h);
        return result>0;
    }
}
