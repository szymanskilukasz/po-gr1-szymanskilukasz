package pl.edu.uwm.wmii.SzymanskiLukasz.Kolokwium02;

import java.time.LocalDateTime;

public abstract class Zamowienie {
    protected LocalDateTime czasDostawy;

    public void UstawCzasDostawy(LocalDateTime czas){
        this.czasDostawy = czas;
    }

    public boolean PoprawnyCzas(){
        int result = czasDostawy.compareTo(LocalDateTime.now());
        return result>0;
    }
}
