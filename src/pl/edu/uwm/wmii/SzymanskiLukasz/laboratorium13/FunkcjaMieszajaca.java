package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium13;

import java.io.*;
import java.util.*;

public class FunkcjaMieszajaca {
    HashMap<Integer, HashSet<String>> hashes;
    public FunkcjaMieszajaca(String file) throws FileNotFoundException{
        this.hashes = new HashMap<>();
        Scanner sc = new Scanner(new File(file));
        while(sc.hasNext()){
            String word = sc.next();
            boolean temp = false;
            for(int hash: this.hashes.keySet()){
                if(hashes.hashCode() == hash){
                    temp = true;
                    break;
                }
            }
            if(!temp){
                this.hashes.put(word.hashCode(),new HashSet<>());
            }
            this.hashes.get(word.hashCode()).add(word);
        }
        this.wypisz();
    }

    public void wypisz(){
        for (int hash: this.hashes.keySet()){
            if(this.hashes.get(hash).size()>1){
                System.out.print(hash+":");
                for(String word: this.hashes.get(hash)){
                    System.out.print(" " + word);
                }
                System.out.print("\n");
            }
        }
    }
    public static void main(String[] args) throws FileNotFoundException {
        FunkcjaMieszajaca funkcjaMieszajaca = new FunkcjaMieszajaca("src/pl/edu/uwm/wmii/kotewa/laboratorium00/Lab_13/test.txt");

    }
}
