package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium02;

import java.util.Scanner;

public class z02_zad_1_c {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int ilosc = 0,max = Integer.MIN_VALUE;
        int[] tab;
        tab=new int[n];
        for(int i=0;i<n;i++){
            tab[i] = -999 + (int)(Math.random() * ((999 - (-999)) + 1));
            if(tab[i] > max) {
                max = tab[i];
            }
        }
        for(int i=0;i<n;i++){
            if(tab[i] == max){
                ilosc++;
            }
        }
        System.out.println("Max: " + max);
        System.out.println("Ilość: " + ilosc);
    }
}