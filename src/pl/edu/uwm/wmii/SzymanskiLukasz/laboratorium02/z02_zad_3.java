package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium02;

import java.util.Arrays;
import java.util.Scanner;

public class z02_zad_3 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int m =in.nextInt(),n =in.nextInt(),k =in.nextInt();
        int[][] a,b,c;
        a = new int[m][n];
        b = new int[n][k];
        c = new int[m][k];

        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                a[j][i] = (int) (Math.random() * (11));
            }
            for(int j=0;j<k;j++){
                b[i][j] = (int) (Math.random() * (11));
            }
        }

        for(int i=0;i<m;i++){
            for(int j=0;j<k;j++){
                c[i][j]=0;
                for(int l=0;l<n;l++){
                    c[i][j]+=a[i][l]*b[l][j];
                }
            }
        }

        System.out.println(Arrays.deepToString(a));
        System.out.println(Arrays.deepToString(b));
        System.out.println(Arrays.deepToString(c));

    }
}
