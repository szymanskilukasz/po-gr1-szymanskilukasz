package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium02;

import java.util.Scanner;

public class z02_zad_1_d {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int sumadod = 0, sumauj = 0;
        int[] tab;
        tab=new int[n];
        for(int i=0;i<n;i++){
            tab[i] = -999 + (int)(Math.random() * ((999 - (-999)) + 1));
            if(tab[i] >= 0){
                sumadod+=tab[i];
            }
            else if (tab[i] < 0){
                sumauj+=tab[i];
            }
        }
        System.out.println("Suma dodatnich: " + sumadod);
        System.out.println("Suma Ujemnych: " + sumauj);
    }
}