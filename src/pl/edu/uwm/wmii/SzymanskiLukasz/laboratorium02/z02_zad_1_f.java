package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium02;

import java.util.Scanner;

public class z02_zad_1_f {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] tab;
        tab=new int[n];
        for(int i=0;i<n;i++){
            tab[i] = -999 + (int)(Math.random() * ((999 - (-999)) + 1));
            if(tab[i] > 0){
                tab[i] = 1;
            }
            else if (tab[i] < 0){
                tab[i] = -1;
            }
            System.out.println(tab[i]);
        }
    }
}