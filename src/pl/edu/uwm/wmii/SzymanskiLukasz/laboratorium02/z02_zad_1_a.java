package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium02;

import java.util.Scanner;

public class z02_zad_1_a {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int parzyste = 0,nieparzyste=0;
        int[] tab;
        tab=new int[n];
        for(int i=0;i<n;i++){
            tab[i] = -999 + (int)(Math.random() * ((999 - (-999)) + 1));
            if(tab[i]%2 == 0){
                parzyste++;
            }
            else {
                nieparzyste++;
            }
        }
        System.out.println("Parzyste: " + parzyste);
        System.out.println("Nieparzyste: " + nieparzyste);
    }
}