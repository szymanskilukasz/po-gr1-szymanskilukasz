package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium02;

import java.util.Scanner;

public class z02_zad_1_b {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int dodatnie = 0,zerowe=0, ujemne=0;
        int[] tab;
        tab=new int[n];
        for(int i=0;i<n;i++){
            tab[i] = -999 + (int)(Math.random() * ((999 - (-999)) + 1));
            if(tab[i] > 0){
                dodatnie++;
            }
            else if (tab[i] < 0){
                ujemne++;
            }
            else {
                zerowe++;
            }
        }
        System.out.println("Dodatnie: " + dodatnie);
        System.out.println("Zerowe: " + zerowe);
        System.out.println("Ujemne: " + ujemne);
    }
}