package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium02;

import java.util.Scanner;

public class z02_zad_2 {

    public static void generuj (int[] tab, int n, int minWartosc, int maxWartosc){
        for(int i=0;i<n;i++) {
            tab[i] = minWartosc + (int) (Math.random() * ((maxWartosc - (minWartosc)) + 1));
        }
    }

    public static int ileNieparzystych (int[] tab){
        int ilosc = 0;
        for(int i : tab) {
            if (i % 2 != 0) {
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int ileParzystych (int[] tab){
        int ilosc = 0;
        for(int i : tab) {
            if (i % 2 == 0) {
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int ileDodatnich (int[] tab){
        int ilosc = 0;
        for(int i : tab){
            if(i > 0){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int ileUjemnych (int[] tab){
        int ilosc = 0;
        for(int i : tab){
            if(i < 0){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int ileZerowych (int[] tab){
        int ilosc = 0;
        for(int i : tab){
            if(i == 0){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int ileMaksymalnych (int[] tab){
        int ilosc = 0,max = Integer.MIN_VALUE;
        for(int i : tab){
            if(i > max) {
                max = i;
            }
        }
        for(int i : tab){
            if(i == max){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int sumaDodatnich (int[] tab) {
        int suma = 0;
        for (int i : tab) {
            if (i >= 0) {
                suma += i;
            }
        }
        return suma;
    }
    public static int sumaUjemnych (int[] tab) {
        int suma = 0;
        for (int i : tab) {
            if (i < 0) {
                suma += i;
            }
        }
        return suma;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich (int[] tab){
        int temp = 0,najdluzszy=0;
        for(int i : tab){
            if(i >= 0){
                temp++;
            }
            else {
                if (temp > najdluzszy){
                    najdluzszy = temp;
                }
                temp = 0;
            }
        }
        if (temp > najdluzszy){
            najdluzszy = temp;
        }
        return najdluzszy;
    }

    public static void signum(int[] tab){
        for(int i=0;i<tab.length;i++){
            if(tab[i] > 0){
                tab[i] = 1;
            }
            else if (tab[i] < 0){
                tab[i] = -1;
            }
            System.out.println(tab[i]);
        }
    }

    public static void odwrocFragment(int[] tab, int lewy, int prawy){
        int[] temp;
        temp=new int[(prawy-lewy)+1];
        for(int i=0;i<=(prawy-lewy);i++){
            temp[(prawy-lewy)-i]=tab[(lewy+i)-1];
        }
        for(int i=0;i<=(prawy-lewy);i++){
            tab[(lewy+i)-1]= temp[i];
        }
    }

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int minWartosc = -999, maxWartosc=999;
        int[] tab;
        tab=new int[n];

        generuj(tab,n,minWartosc,maxWartosc);
        System.out.println("parzyste : " + ileParzystych(tab));
        System.out.println("nieparzyste : " + ileNieparzystych(tab));
        System.out.println("dodatnie : " + ileDodatnich(tab));
        System.out.println("ujemne : " + ileUjemnych(tab));
        System.out.println("zerowe : " + ileZerowych(tab));
        System.out.println("ile maksymalnych : " + ileMaksymalnych(tab));
        System.out.println("suma dodatnich : " + sumaDodatnich(tab));
        System.out.println("suma ujemnych : " + sumaUjemnych(tab));
        System.out.println("dlugosc maksymalnego ciagu dodatnich : " + dlugoscMaksymalnegoCiaguDodatnich(tab));
        odwrocFragment(tab,in.nextInt(),in.nextInt());
        signum(tab);
    }
}
