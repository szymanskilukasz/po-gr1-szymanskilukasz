package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium12;

public class LinkedList {
    public LinkedList() {
        size = 0;

    }

    public static void main(String[] args)
    {
        LinkedList list = new LinkedList();

        list.add("Maks");
        list.add("Jakub");
        list.add("Szymon");
        list.add("Alan");
        list.add("Robert");
        list.add("Adrian");

        list.show();
        list.freeList();

    }


    Node head;
    int size;

    static class Node {

        Object data;
        Node next;

        Node(Object data)
        {
            this.data = data;
            next = null;
        }
    }

    public int getSize(){
        return this.size;
    }

    public void add(Object data)
    {
        Node new_node = new Node(data);
        new_node.next = null;

        if(this.head == null) {
            this.head = new_node;
            this.size += 1;
        }
        else {

            Node last = this.head;
            while (last.next != null) {
                last = last.next;
            }

            last.next = new_node;
            this.size += 1;
        }
    }

    public void show()
    {
        Node currNode = this.head;

        System.out.print("head -->");

        while (currNode != null) {
            System.out.printf("[%s]>>",currNode.data);

            currNode = currNode.next;
        }
        System.out.print("null\n");

    }
    public Node freeList()
    {
        Node curr = this.head;
        while (curr != null)
        {
            Node next = curr.next;
            curr = next;
        }
        return curr;
    }

    public static void redukuj(LinkedList nodes, int n){
        if(nodes.getSize()>0 && n>=2){
            Node temp = nodes.head, prev = null;
            int count = 0;
            while (temp != null){
                count++;

                if(n == count){
                    prev.next = temp.next;
                    count = 0;
                }

            }
            if(count != 0)
                prev = temp;
            temp = prev.next;

        }else{
            nodes.freeList();
        }

    }

}
