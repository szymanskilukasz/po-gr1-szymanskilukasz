package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium04;

import java.util.Arrays;
import java.util.Scanner;

public class z04_zad_1 {

    public static int countChar(String str, char c){
        int ilosc = 0;
        for(int i = 0; i<str.length(); i++){
            if(str.charAt(i) == c){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int countSubStr(String str, String subStr){
            int ilosc = 0,subIlosc;
            for(int i = 0; i<str.length(); i++){
                if(str.charAt(i) == subStr.charAt(0)){
                    subIlosc = 0;
                    for(int j=0; j<subStr.length();j++){
                        if(subStr.length() <= str.length() - i){
                            if(str.charAt(i+j)==subStr.charAt(j)){
                                subIlosc++;
                            }
                        }
                    }
                    if(subIlosc == subStr.length()){
                        ilosc++;
                    }
                }
            }
            return ilosc;
        }

    public static String middle(String str){
        String middle;
        int len = str.length();
        if(str.length()%2 == 0){
            middle = Character.toString(str.charAt((len/2)-1)) + str.charAt(len / 2);
        }
        else {
            middle = Character.toString(str.charAt((int) Math.floor((double) len/2)));
        }
        return middle;
    }

    public static String repeat(String str, int n){
        if(n==0){
            return "";
        }
        return str + repeat(str,(n-1));
    }
    
    public static int[] where(String str, String subStr){
        int[] tab;
        int ilosc = 0,subIlosc;
        for(int i = 0; i<str.length(); i++){
            if(str.charAt(i) == subStr.charAt(0)){
                subIlosc = 0;
                for(int j=0; j<subStr.length();j++){
                    if(subStr.length() <= str.length() - i){
                        if(str.charAt(i+j)==subStr.charAt(j)){
                            subIlosc++;
                        }
                    }
                }
                if(subIlosc == subStr.length()){
                    ilosc++;
                }
            }
        }
        tab = new int[ilosc*2];
        ilosc = 0;
        for(int i = 0; i<str.length(); i++){
            if(str.charAt(i) == subStr.charAt(0)){
                subIlosc = 0;
                for(int j=0; j<subStr.length();j++){
                    if(subStr.length() <= str.length() - i){
                        if(str.charAt(i+j)==subStr.charAt(j)){
                            subIlosc++;
                        }
                    }
                }
                if(subIlosc == subStr.length()){
                    tab[ilosc*2] = i;
                    tab[(ilosc*2)+1] = i + subStr.length()-1;
                    ilosc++;
                }
            }
        }
        return tab;
    }

    public static String change(String str){
        StringBuilder zmieniony = new StringBuilder();
        for(int i = 0;i<str.length();i++){
            if(Character.isUpperCase(str.charAt(i))){
                zmieniony.append(Character.toLowerCase(str.charAt(i)));
            }
            else {
                zmieniony.append(Character.toUpperCase(str.charAt(i)));
            }
        }
        return zmieniony.toString();
    }

    public static String nice(String str){
        StringBuilder zmieniony = new StringBuilder();
        int licznik = (str.length()-1)%3;
        zmieniony.append(str.charAt(0));
        for(int i = 1;i<str.length();i++){
            if(licznik==0){
                zmieniony.append("'");
                licznik=3;
            }
            zmieniony.append(str.charAt(i));
            licznik--;
        }
        return zmieniony.toString();
    }

    public static String nice2(String str,char c, int n){
        StringBuilder zmieniony = new StringBuilder();
        int licznik = (str.length()-1)%n;
        zmieniony.append(str.charAt(0));
        for(int i = 1;i<str.length();i++){
            if(licznik==0){
                zmieniony.append(c);
                licznik=n;
            }
            zmieniony.append(str.charAt(i));
            licznik--;
        }
        return zmieniony.toString();
    }

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);

        //a
        System.out.println("a) podaj wyraz oraz znak");
        String str = in.nextLine();
        char c = in.next().charAt(0);
        in.nextLine();
        System.out.println(countChar(str,c));

        //b
        System.out.println("a) podaj dwa wyrazy");
        str = in.nextLine();
        String subStr = in.nextLine();
        System.out.println(countSubStr(str,subStr));

        //c
        System.out.println("c) podaj wyraz");
        str = in.nextLine();
        System.out.println(middle(str));

        //d
        System.out.println("d) podaj wyraz i liczbe");
        str = in.nextLine();
        int n = in.nextInt();
        in.nextLine();
        System.out.println(repeat(str,n));

        //e
        System.out.println("e) podaj dwa wyrazy");
        str = in.nextLine();
        subStr = in.nextLine();
        System.out.println(Arrays.toString(where(str, subStr)));

        //f
        System.out.println("e) podaj wyraz");
        str = in.nextLine();
        System.out.println(change(str));

        //g
        System.out.println("e) podaj liczbe calkowita");
        str = in.nextLine();
        System.out.println(nice(str));

        //h
        System.out.println("e) podaj liczbe calkowita, separator i liczbe pozycji miedzy separatorem");
        str = in.nextLine();
        c = in.next().charAt(0);
        n = in.nextInt();
        System.out.println(nice2(str,c,n));

    }
}
