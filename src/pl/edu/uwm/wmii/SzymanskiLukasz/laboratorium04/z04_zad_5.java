package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium04;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class z04_zad_5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj kapitał, stopę procentową i ilość lat");
        BigDecimal kapital = in.nextBigDecimal();
        BigDecimal odsetki = in.nextBigDecimal();
        int lata = in.nextInt();

        for(int i=0;i<lata;i++){
            kapital = kapital.add(kapital.multiply(odsetki));
            kapital = kapital.setScale(2, RoundingMode.DOWN);
        }
        System.out.println(kapital);
    }
}
