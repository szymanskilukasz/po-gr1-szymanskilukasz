package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class z04_zad_3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String data;
        String nazwa = in.nextLine();
        String subStr = in.nextLine();
        int zlicz = 0,subZlicz;
        try{

            File plik = new File("src/pl/edu/uwm/wmii/SzymanskiLukasz/laboratorium04/"+nazwa);
            Scanner f = new Scanner(plik);
            while(f.hasNextLine()){
                data = f.nextLine();
                for(int i = 0; i<data.length();i++){
                    if(subStr.length() <= data.length() - i){
                        if(data.charAt(i)==subStr.charAt(0)){
                            subZlicz = 0;
                            for(int j=0; j<subStr.length();j++){
                                if(data.charAt(i+j) == subStr.charAt(j)){
                                    subZlicz++;
                                }
                            }
                            if(subZlicz==subStr.length()){
                                if((i==0 || data.charAt(i-1) == ' ' ) &&
                                        (i+subStr.length() == data.length() || data.charAt(i+subStr.length())==' ' )){
                                    zlicz++;
                                }
                            }
                        }
                    }

                }
            }
            f.close();
        }
        catch (FileNotFoundException error){
            System.out.println("Nie znaleziono pliku");
            error.printStackTrace();
        }
        System.out.println(zlicz);
    }
}
