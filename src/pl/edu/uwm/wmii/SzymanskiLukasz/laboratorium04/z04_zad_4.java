package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium04;

import java.math.BigInteger;
import java.util.Scanner;

public class z04_zad_4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj n");
        int n = in.nextInt();
        n *=n;
        BigInteger suma = new BigInteger("0");
        BigInteger pole = new BigInteger("1");
        BigInteger mnoznik = new BigInteger("2");
        for(int i=0;i<n;i++){
            suma = suma.add(pole);
            pole=pole.multiply(mnoznik);
        }
        System.out.println(suma);
    }
}
