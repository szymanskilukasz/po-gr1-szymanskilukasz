package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class z04_zad_2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String data;
        String nazwa = in.nextLine();
        char c = in.next().charAt(0);
        int zlicz = 0;
        try{

            File plik = new File("src/pl/edu/uwm/wmii/SzymanskiLukasz/laboratorium04/"+nazwa);
            Scanner f = new Scanner(plik);
            while(f.hasNextLine()){
                data = f.nextLine();
                for(int i = 0; i<data.length();i++){
                    if(data.charAt(i)==c){
                        zlicz++;
                    }
                }
            }
            f.close();
        }
        catch (FileNotFoundException error){
            System.out.println("Nie znaleziono pliku");
            error.printStackTrace();
        }
        System.out.println(zlicz);
    }
}
