package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium00;

public class z00_zad_7 {
    public static void main(String[] args){
        String tekst = """
                          X       X        X   X                                                                          X     X                            X
                          X       X        X   X    X       XXXX      XXXX     XXXXXXXXX         XXXX       X XXXXXXX     X    XX   XX  X      XXXX      X   X       XXXX X       X
                          X       X        X   X   XX       X   X   XXX              XXX        XX                 XX      XX XX   XXX  XX    X   XX     XX      X   X X          X      X       X
                          X X     X       XX   X   X        X   X   X                X          XX                X         XXX    X XXX X    X    X     X X    XX     XXXX       X   XXX        X
                         XXX      X       X    X XX        X    X   XXX            XX             XXX             X          X     X  XX X   X     X     X  X   X          XX     X  XX          X
                        X X       XX      X    XXX        XXXXXXX     XXXX        XX                XX          XX           X     X     X   XXXXXXX     X  XX XX           XX    X XX           X
                          X        X     XX    XXXXXX     X     X        X       XX                   X        X             X    X       X  X     X     X   X X             X    XXXXXX         XX
                          X        X    XX     X    XX    X     X       XX      XX                   XX       XX             X    X       X  X     X     X    XX            XX    XX    XXX       X
                          XXXXX     XXXXX      X     X    X         XXXXX      XXXXXXX          XXXXXX       XXXXXX          X    X          X     X     X    X       XXXXXXX      X      XX      X
                                                                                                             XX              X    X                                                X              X
                                
                """;
        System.out.println(tekst);
    }
}
