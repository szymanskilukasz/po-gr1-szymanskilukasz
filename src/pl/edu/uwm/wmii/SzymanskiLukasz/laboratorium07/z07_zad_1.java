package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium07;
import pl.imiajd.szymanski.*;

public class z07_zad_1 {
    public static void main(String[] args) {
        NazwanyPunkt a = new NazwanyPunkt(3, 5, "port");
        a.show();

        Punkt b = new Punkt(3, 5);
        b.show();

        Punkt c = new NazwanyPunkt(3, 6, "tawerna");
        c.show();
        //a = b;               //  --- powoduje błąd kompilacji  (dlaczego ?) - próbujemy przypisać klasę punkt do klasy nazwany punkt
        //a = (NazwanyPunkt) b;  //   --- powoduje błąd wykonania   (dlaczego ?) - punkt b nie ma nazwy, więc będzie tam wartość ClassCastException

        //a = c;                 //--- powoduje błąd kompilacji  (dlaczego ?) - próbujemy przypisać klasę punkt do klasy nazwany punkt
        a = (NazwanyPunkt) c;
        a.show();
    }
}
