package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium07;

import pl.imiajd.szymanski.Adres;

public class z07_zad_2 {
    public static void main(String[] args) {
        Adres a1 = new Adres("10-089", "Olsztyn", "Warszawska", 10, 12);
        Adres a2 = new Adres("10-099", "Olsztyn", "Pstrowskiego", 20);
        a1.pokaz();
        a2.pokaz();
        System.out.println(a1.przed(a2));
    }
}
