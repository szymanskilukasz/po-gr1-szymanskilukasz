package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium07;

import pl.imiajd.szymanski.Nauczyciel;
import pl.imiajd.szymanski.Osoba;
import pl.imiajd.szymanski.Student;

public class z07_zad_4 {
    public static void main(String[] args) {
        Osoba o1 = new Osoba("Kowalski", 1997);
        System.out.println(o1.getNazwisko());
        System.out.println(o1.getRok_urodzenia());
        System.out.println(o1);
        Student s1 = new Student("Nowak", 1999, "Informatyka");
        System.out.println(s1);
        Nauczyciel n1 = new Nauczyciel("Zielony", 1965,3000);
        System.out.println(n1);
    }
}
