package pl.edu.uwm.wmii.SzymanskiLukasz.kolokwium01;

import java.util.Scanner;

public class zad_3 {
    public static int min(int[] tab){
        int minimum = Integer.MAX_VALUE;
        int minindex = 0;
        for(int i = 9; i>=0; i--){
            if(tab[i]<=minimum){
                minimum = tab[i];
                minindex = i;
            }
        }
        return minindex;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] tab = new int[10];
        for (int i = 0; i<10; i++){
            tab[i] = in.nextInt();
        }
        System.out.println("indeks najmniejszego elementu : " + min(tab));
    }
}
