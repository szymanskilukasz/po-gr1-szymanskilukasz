package pl.edu.uwm.wmii.SzymanskiLukasz.kolokwium01;

import java.util.Scanner;

public class zad_4 {
    public static int maxuj(int[] tab){
        int maximum = Integer.MIN_VALUE;
        int maxindex = 1111;
        for(int i = 9; i>=0; i--){
            if(tab[i] >= maximum && tab[i] < 0){
                maximum = tab[i];
                maxindex = i;
            }
        }
        if(maxindex == 1111){
            System.out.println("brak liczb ujemnych!!!");
        }
        return maxindex;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] tab = new int[10];
        for (int i = 0; i<10; i++){
            tab[i] = in.nextInt();
        }
        System.out.println("indeks największej ujemnej liczby : " + maxuj(tab));
    }
}
