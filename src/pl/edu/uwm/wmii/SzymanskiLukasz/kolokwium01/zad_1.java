package pl.edu.uwm.wmii.SzymanskiLukasz.kolokwium01;

import java.util.Scanner;

public class zad_1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double a = in.nextDouble();
        double c  = in.nextDouble();
        double delta = Math.pow(2,2) - 4.0 * a * (-3.0) * c;
        if(delta>0){
            System.out.println("x1 : " + ((-2.0) - Math.sqrt(delta))/(2*a));
            System.out.println("x2 : " + ((-2.0) + Math.sqrt(delta))/(2*a));
        }
        else if (delta == 0){
            System.out.println("x0 : " + ((-2.0)/(2*a)));
        }
        else{
            System.out.println("funkcja nie ma pierwiastków");
        }
    }
}
