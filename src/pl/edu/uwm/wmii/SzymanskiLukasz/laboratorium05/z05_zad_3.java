package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium05;

import java.util.ArrayList;

public class z05_zad_3 {
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik = new ArrayList<>();
        int amin, bmin, aindex = 0, bindex = 0;
        while(!a.isEmpty() || !b.isEmpty()) {
            amin = Integer.MAX_VALUE;
            bmin = Integer.MAX_VALUE;
            for (int i = 0; i < a.size(); i++) {
                if (a.get(i) < amin) {
                    amin = a.get(i);
                    aindex = i;
                }
            }
            for (int i = 0; i < b.size(); i++) {
                if (b.get(i) < bmin) {
                    bmin = b.get(i);
                    bindex = i;
                }
            }
            if(amin<=bmin){
                wynik.add(amin);
                a.remove(aindex);
            }
            else {
                wynik.add(bmin);
                b.remove(bindex);
            }
        }
        return wynik;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        System.out.println(mergeSorted(a, b));
    }
}
