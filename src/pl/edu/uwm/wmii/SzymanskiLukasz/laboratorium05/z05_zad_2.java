package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium05;

import java.util.ArrayList;

public class z05_zad_2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik = new ArrayList<>();
        if(a.size()>=b.size()){
            for(int i =0;i<b.size();i++){
                wynik.add(a.get(i));
                wynik.add(b.get(i));
            }
            for(int i = b.size();i<a.size();i++){
                wynik.add(a.get(i));
            }
        }
        else {
            for(int i =0;i<a.size();i++){
                wynik.add(a.get(i));
                wynik.add(b.get(i));
            }
            for(int i = a.size();i<b.size();i++){
                wynik.add(b.get(i));
            }
        }

        return wynik;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        System.out.println(merge(a, b));
    }
}
