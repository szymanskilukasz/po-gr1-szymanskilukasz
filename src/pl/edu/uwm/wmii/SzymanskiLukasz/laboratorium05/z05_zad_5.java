package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium05;

import java.util.ArrayList;

public class z05_zad_5 {
    public static void reverse(ArrayList<Integer> a){
        ArrayList<Integer> temp = new ArrayList<>();
        for(int i = a.size()-1;i>=0;i--){
            temp.add(a.get(i));
        }
        for(int i = 0;i<a.size();i++){
            a.set(i,temp.get(i));
        }
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        a.add(9);
        reverse(a);
        System.out.println(a);
    }
}
