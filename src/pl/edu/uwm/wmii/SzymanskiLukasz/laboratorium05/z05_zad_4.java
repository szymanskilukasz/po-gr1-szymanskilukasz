package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium05;

import java.util.ArrayList;

public class z05_zad_4 {
    public static ArrayList<Integer> reverse(ArrayList<Integer> a){
        ArrayList<Integer> wynik = new ArrayList<>();
        for(int i = a.size()-1;i>=0;i--){
            wynik.add(a.get(i));
        }
        return wynik;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        a.add(9);
        System.out.println(reverse(a));
    }
}
