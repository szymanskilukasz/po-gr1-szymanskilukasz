package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium05;

import java.util.ArrayList;

public class z05_zad_1 {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik = new ArrayList<>();
        wynik.addAll(a);
        wynik.addAll(b);
        return wynik;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        System.out.println(append(a, b));
    }
}
