package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium06;


public class IntegerSet {
    public boolean[] zbior;
    public IntegerSet(){
        zbior =  new boolean[100];
    }

    public static IntegerSet union(IntegerSet a, IntegerSet b){
        IntegerSet suma = new IntegerSet();
        for(int i = 0; i<99; i++){
            if(a.zbior[i] || b.zbior[i]){
                suma.zbior[i] = true;
            }
        }
        return suma;
    }

    public static IntegerSet intersection(IntegerSet a, IntegerSet b){
        IntegerSet suma = new IntegerSet();
        for(int i = 0; i < 99; i++){
            if(a.zbior[i] && b.zbior[i]){
                suma.zbior[i] = true;
            }
        }
        return suma;
    }

    public void insertElement(int n){
        zbior[n] = true;
    }

    public void deleteElement(int n){
        zbior[n] = false;
    }

    @Override
    public String toString(){
        StringBuilder text = new StringBuilder();
        for(int i = 0; i < 99; i++){
            if(zbior[i]){
                text.append(i).append(" ");
            }
        }
        return text.toString();
    }

    public boolean equals(IntegerSet b){
        boolean wynik = true;
        for(int i = 0; i < 99; i++){
            if (b.zbior[i] != zbior[i]) {
                wynik = false;
                break;
            }
        }
        return wynik;
    }


    public static void main(String[] args) {

       IntegerSet s1 = new IntegerSet();
       IntegerSet s2 = new IntegerSet();

       s1.insertElement(10);
       s1.insertElement(20);
       s1.insertElement(22);
       s2.insertElement(10);
       s2.insertElement(20);
       s2.insertElement(24);

       System.out.println(union(s1, s2));

       s2.deleteElement(20);

       System.out.println(intersection(s1, s2));

       System.out.println(s1);

       System.out.println(s1.equals(s2));
    }
}
