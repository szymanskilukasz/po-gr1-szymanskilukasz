package pl.edu.uwm.wmii.SzymanskiLukasz.laboratorium06;

public class RachunekBankowyTest {
    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("saver1 = "+ saver1.saldo);
        System.out.println("saver2 = "+ saver2.saldo);
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("saver1 = "+ saver1.saldo);
        System.out.println("saver2 = "+ saver2.saldo);
    }
}

class RachunekBankowy {
    public static double rocznaStopaProcentowa;
    public double saldo;

    public RachunekBankowy(double money){
        saldo = money;
    }

    public void obliczMiesieczneOdsetki(){
        saldo += (saldo * rocznaStopaProcentowa)/12;
        saldo = Math.round(saldo*100)/100.0;
    }

    public static void setRocznaStopaProcentowa(double value){
        rocznaStopaProcentowa = value;
    }
}


